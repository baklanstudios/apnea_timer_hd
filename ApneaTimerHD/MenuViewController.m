//
//  MenuViewController.m
//  ApneaTimerHD
//
//  Created by Mikhail on 08.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "MenuViewController.h"
#import "NSTimer+Block.h"
#import "MFSideMenu.h"

@interface MenuViewController ()
{
    NSArray *tables;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSTimer *hideTimeout;

@end


@implementation MenuViewController

#pragma mark - Defines
#define HIDE_MENU_TIMEOUT 2.0

#pragma mark - View Controller

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Training Tables", nil);
    self.tableView.delegate   = self;
    self.tableView.dataSource = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cancelHideTimeout:)
                                                 name:@"MFSideMenuStateNotificationEvent"
                                               object:nil];
}

-(void) cancelHideTimeout:(NSNotification *) notification {
    int evt = [(NSNumber*)[notification.userInfo valueForKey:@"eventType"] intValue];
    /*
    switch (evt) {
        case MFSideMenuStateEventMenuDidClose:
            NSLog(@"did close");
            break;
        case MFSideMenuStateEventMenuWillClose:
            NSLog(@"will close");
            break;
        case MFSideMenuStateEventMenuDidOpen:
            NSLog(@"did open");
            break;
        case MFSideMenuStateEventMenuWillOpen:
            NSLog(@"will open");
            break;
        default:
            NSLog(@"unknwn event");
            break;
    }
    */
    if (evt == MFSideMenuStateEventMenuWillClose) {
        if(self.hideTimeout) {
            [self.hideTimeout invalidate];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tables)
        return tables.count;
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TrainingTypeCell" forIndexPath:indexPath];
    NSString *object = (tables) ? tables[indexPath.row] : @"none";
    cell.textLabel.text = [object description];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate selectedTableIndex:(int)indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    // hide menu by timeout
    if (self.hideTimeout)
        [self.hideTimeout invalidate];
    self.hideTimeout = [NSTimer scheduledTimerWithTimeInterval:HIDE_MENU_TIMEOUT
                                    repeats:NO
                                      block:(
        ^{
            if(self.menuContainerViewController.menuState != MFSideMenuStateClosed) {
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            }
        })];
}

-(void)setDataTable:(NSArray*)tableNames {
    tables = tableNames;
}




/*
 CATransition *animation = [CATransition animation];
 animation.duration = .33;
 animation.type = kCATransitionFade;
 animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
 [self.appDelegate.rightViewController.lblTitle.layer addAnimation:animation forKey:@"changeTextTransition"];
 */

//    [self hideMaster];
//    [self.appDelegate.splitViewController.view setNeedsLayout];
//    [self.appDelegate.splitViewController willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];

//    self.appDelegate.rightViewController.lblTitle.text = title;
//    self.appDelegate.rightViewController.navigationItem.title = title;


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
