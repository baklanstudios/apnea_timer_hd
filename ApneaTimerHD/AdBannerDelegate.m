//
//  AdBannerDelegate.m
//  ApneaTimerHD
//
//  Created by Mikhail on 10.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "AdBannerDelegate.h"
#import "AppDelegate.h"
#import "MFSideMenuContainerViewController.h"

@interface AdBannerDelegate()

@property (weak, nonatomic) AppDelegate *appDelegate;

@end

@implementation AdBannerDelegate

-(AdBannerDelegate*) init {
    self = [super init];
    if (self) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

        self.bannerVisible = NO;
        
        //if (!self.appDelegate.purchased) {
        self.banner = [[ADBannerView alloc] init];
        self.banner.delegate = self;
        //}
        
    }
    return self;
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
//    NSLog(@"banner load");
    if (!self.bannerVisible) {
        self.bannerVisible = YES;
        MFSideMenuContainerViewController *swc = (MFSideMenuContainerViewController*)[self.appDelegate.window rootViewController];
        UINavigationController *nvc = (UINavigationController*)swc.centerViewController;
        [[nvc topViewController].view setNeedsUpdateConstraints];
    }
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
//    NSLog(@"banner fail");
    /*
     [UIView beginAnimations:@"animateAdBannerOn" context:NULL];
     self.adBanner.frame = CGRectOffset(self.adBanner.frame,
     0,
     self.adBanner.frame.size.height
     + [[self.tabBarVC tabBar] bounds].size.height);
     [[self getCurrentViewController] expandView:NO];
     [UIView commitAnimations];
     */
//    NSLog(@"fail to receive ad");
}

- (BOOL) bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave {
//    if ([self.appDelegate.timer isActive]) {
//        [self.appDelegate.timer pause];
//    }
    return YES;
}

- (void) bannerViewActionDidFinish:(ADBannerView *)banner {
//    if (self.appDelegate.timer.isPaused) {
//        [self.appDelegate.timer start];
//    }
}

@end
