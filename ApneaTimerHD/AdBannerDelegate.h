//
//  AdBannerDelegate.h
//  ApneaTimerHD
//
//  Created by Mikhail on 10.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <iAd/iAd.h>

@interface AdBannerDelegate : NSObject <ADBannerViewDelegate>

-(AdBannerDelegate*) init;

#pragma mark - iAd support
@property BOOL bannerVisible;
@property (strong, nonatomic) ADBannerView *banner;

@end
