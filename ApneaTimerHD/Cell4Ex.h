//
//  Cell4Ex.h
//  ApneaTimerHD
//
//  Created by Mikhail on 12.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "Cell4.h"

static NSString * const idCell4Ex = @"Cell4Ex";

@interface Cell4Ex : Cell4

@property (nonatomic, strong) UILabel *lblCycles;

@end
