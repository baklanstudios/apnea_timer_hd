//
//  Cell2.h
//  ApneaTimerHD
//
//  Created by Mikhail on 12.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CellCommon.h"

static NSString * const idCell2 = @"Cell2";

@interface Cell2 : CellCommon

@property (nonatomic, strong) UILabel *lblDuration1;
@property (nonatomic, strong) UILabel *lblDuration2;
@property (nonatomic, strong) UILabel *lblDescription1;
@property (nonatomic, strong) UILabel *lblDescription2;

@end
