//
//  MainViewControllerTimer.h
//  ApneaTimerHD
//
//  Created by Mikhail on 08.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "MainViewControllerPrototype.h"
#import "TrainingTable.h"

@interface MainViewControllerTimer : MainViewControllerPrototype <UITableViewDelegate, UITableViewDataSource, TimerViewProtocol, AVAudioPlayerDelegate>

@property (strong, nonatomic) NSString *title;
@property BOOL hideRepeatsSlider;

//-(void) setRepeatsSlider;
-(void) setControls;

@end
