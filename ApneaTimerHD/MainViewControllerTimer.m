//
//  MainViewControllerTimer.m
//  ApneaTimerHD
//
//  Created by Mikhail on 08.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "MainViewControllerTimer.h"
#import "AppDelegate.h"
#import "TrainingTableEx.h"
#import "TrainingTable.h"

#import "CellManager.h"
#import "TextColorDelegate.h"

#import "Tools.h"

#import "Flurry.h"

@interface MainViewControllerTimer ()

{
    UIColor *green;
    UIColor *blue;
    UIColor *red;
    UIColor *orange;
    UIColor *grey;
    UIColor *lightGrey;
    UIColor *sliderColor;
    BOOL canPlay;
    int  lastPhase;
}

@property (weak, nonatomic) IBOutlet UIView  *viewControls;
@property (weak, nonatomic) IBOutlet UIView  *viewTable;

@property (strong, nonatomic) UILabel        *lblPhaseTitle;
@property (strong, nonatomic) UILabel        *lblPhaseTime;
@property (strong, nonatomic) UIProgressView *progress;
@property (strong, nonatomic) UILabel        *lblNextPhaseTitle;
@property (strong, nonatomic) UILabel        *lblTimeLeftTitle;
@property (strong, nonatomic) UILabel        *lblNextPhaseValue;
@property (strong, nonatomic) UILabel        *lblTimeLeftValue;
@property (strong, nonatomic) UIButton       *btnStart;
@property (strong, nonatomic) UILabel        *lblStub;
@property (strong, nonatomic) UIButton       *btnSkip;

@property (strong, nonatomic) UITableView    *tableView;
@property (strong, nonatomic) UILabel        *lblTimeBase;
@property (strong, nonatomic) UISlider       *sldrTimeBase;
@property (strong, nonatomic) UILabel        *lblRepeats;
@property (strong, nonatomic) UISlider       *sldrRepeats;


@property (strong, nonatomic) AVAudioPlayer *player;

@end

@implementation MainViewControllerTimer

static float timerStep = 0.033f;

#pragma mark - Initialization

-(void) createControls {
    
    // create TimerView controls
    self.lblPhaseTitle = [[UILabel alloc] init];
    self.lblPhaseTitle.text = @"PhaseTitle";
    self.lblPhaseTitle.font = [UIFont systemFontOfSize:25];
    self.lblPhaseTitle.textAlignment = NSTextAlignmentCenter;
    [self.viewControls addSubview:self.lblPhaseTitle];

    self.lblPhaseTime = [[UILabel alloc] init];
    self.lblPhaseTime.text = @"00:00:00";
    self.lblPhaseTime.font = [UIFont systemFontOfSize:40];
    self.lblPhaseTime.textAlignment = NSTextAlignmentCenter;
    [self.viewControls addSubview:self.lblPhaseTime];

    self.progress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    [self.viewControls addSubview:self.progress];

    self.lblNextPhaseTitle = [[UILabel alloc] init];
    self.lblNextPhaseTitle.text = NSLocalizedString(@"Next Phase Title", nil);
    self.lblNextPhaseTitle.font = [UIFont systemFontOfSize:22];
    self.lblNextPhaseTitle.textAlignment = NSTextAlignmentLeft;
    [self.viewControls addSubview:self.lblNextPhaseTitle];

    self.lblTimeLeftTitle = [[UILabel alloc] init];
    self.lblTimeLeftTitle.text = NSLocalizedString(@"Total Time Left", nil);
    self.lblTimeLeftTitle.font = [UIFont systemFontOfSize:22];
    self.lblTimeLeftTitle.textAlignment = NSTextAlignmentLeft;
    [self.viewControls addSubview:self.lblTimeLeftTitle];
    
    self.lblNextPhaseValue = [[UILabel alloc] init];
    self.lblNextPhaseValue.text = @"Next Phase Value";
    self.lblNextPhaseValue.font = [UIFont systemFontOfSize:22];
    self.lblNextPhaseValue.textAlignment = NSTextAlignmentRight;
    [self.viewControls addSubview:self.lblNextPhaseValue];
    
    self.lblTimeLeftValue = [[UILabel alloc] init];
    self.lblTimeLeftValue.text = @"Time Left Value";
    self.lblTimeLeftValue.font = [UIFont systemFontOfSize:22];
    self.lblTimeLeftValue.textAlignment = NSTextAlignmentRight;
    [self.viewControls addSubview:self.lblTimeLeftValue];

    
    self.lblStub = [[UILabel alloc] init];
    [self.viewControls addSubview:self.lblStub];
    
    CGRect rc = CGRectMake(0, 0, 120, 120);
    self.btnStart.frame = rc;

    self.btnStart = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.btnStart setTitle:NSLocalizedString(@"Start", nil) forState:UIControlStateNormal];
    self.btnStart.frame = rc;
    self.btnStart.titleLabel.font = [UIFont systemFontOfSize:24];
    [[self.btnStart layer] setMasksToBounds:YES];
    [[self.btnStart layer] setCornerRadius:rc.size.width / 2];
    [[self.btnStart layer] setBorderWidth:2.0f];
    [self.btnStart addTarget:self
                      action:@selector(btnStartPressed:)
            forControlEvents:UIControlEventTouchUpInside];
    [self.viewControls addSubview:self.btnStart];

    self.btnSkip = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.btnSkip setTitle:NSLocalizedString(@"Reset", nil) forState:UIControlStateNormal];
    self.btnSkip.frame = rc;
    self.btnSkip.titleLabel.font = [UIFont systemFontOfSize:24];
    [[self.btnSkip layer] setMasksToBounds:YES];
    [[self.btnSkip layer] setCornerRadius:rc.size.width / 2];
    [[self.btnSkip layer] setBorderWidth:2.0f];
    [self.btnSkip addTarget:self
                      action:@selector(btnSkipPressed:)
            forControlEvents:UIControlEventTouchUpInside];
    [self.viewControls addSubview:self.btnSkip];
    
    // create TableView controls
    self.tableView = [[UITableView alloc] initWithFrame:self.viewTable.bounds style:UITableViewStylePlain];
    self.tableView.delegate   = self;
    self.tableView.dataSource = self;
    self.tableView.allowsSelection = NO;
    [self.tableView setRowHeight:65];
    [CellManager registerCellTypes:self.tableView];
//        self.tableView.backgroundColor = [UIColor cyanColor];
    [self.tableView reloadData];
    [self.viewTable addSubview:self.tableView];
    
    self.lblTimeBase = [[UILabel alloc] init];
    self.lblTimeBase.text = NSLocalizedString(@"Time Base", nil);
    //self.lblTimeBase.font = [UIFont systemFontOfSize:22];
    self.lblTimeBase.textAlignment = NSTextAlignmentCenter;
    [self.viewTable addSubview:self.lblTimeBase];
    
    self.sldrTimeBase = [[UISlider alloc] init];
    [self.sldrTimeBase addTarget:self
                          action:@selector(sliderTimeBaseAction:)
                forControlEvents:UIControlEventValueChanged];
    [self.sldrTimeBase setBackgroundColor:[UIColor clearColor]];
    self.sldrTimeBase.minimumValue = 5.0;
    self.sldrTimeBase.maximumValue = 15.0;
    self.sldrTimeBase.continuous = YES;
    self.sldrTimeBase.value = 10.0;
    [self.viewTable addSubview:self.sldrTimeBase];
    
    self.lblRepeats = [[UILabel alloc] init];
    self.lblRepeats.text = NSLocalizedString(@"Repeats", nil);
    //self.lblRepeats.font = [UIFont systemFontOfSize:22];
    self.lblRepeats.textAlignment = NSTextAlignmentCenter;
    [self.viewTable addSubview:self.lblRepeats];
    
    self.sldrRepeats = [[UISlider alloc] init];
    [self.sldrRepeats addTarget:self
                         action:@selector(sliderRepeatsAction:)
               forControlEvents:UIControlEventValueChanged];
    [self.sldrRepeats setBackgroundColor:[UIColor clearColor]];
    self.sldrRepeats.minimumValue = 1.0;
    self.sldrRepeats.maximumValue = 15.0;
    self.sldrRepeats.continuous = YES;
    self.sldrRepeats.value = 10.0;
    [self.viewTable addSubview:self.sldrRepeats];
}

-(void) initColors {
    green     = [UIColor colorWithRed:(97/255.0)    green:(160/255.0)   blue:(5/255.0)      alpha:1];
    orange    = [UIColor colorWithRed:(255/255.0)   green:(164/255.0)   blue:(96/255.0)     alpha:1];;
    red       = [UIColor colorWithRed:(255/255.0)   green:(99/255.0)    blue:(71/255.0)     alpha:1];
    lightGrey = [UIColor colorWithRed:(139/255.0)   green:(136/255.0)   blue:(120/255.0)    alpha:1];
    blue      = [UIColor colorWithRed:(100/255.0)   green:(149/255.0)   blue:(237/255.0)    alpha:1];
    grey      = [UIColor colorWithRed:(97/255.0)    green:(160/255.0)   blue:(5/255.0)      alpha:1];
}

#pragma mark - View Controller

- (void)viewDidLoad {
    [super viewDidLoad];
    lastPhase = -1;
    [self  createControls];
    [self  initColors];
    [self  enableStartButton];
    [self  disableResetButton];
    sliderColor = self.sldrTimeBase.tintColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setControls];
}

- (void)willAnimateRotationToInterfaceOrientation:
(UIInterfaceOrientation)toInterfaceOrientation
                                         duration:(NSTimeInterval)duration {
    [self.view setNeedsUpdateConstraints];
}

-(void) updateViewConstraints {
    [super updateViewConstraints];
    [self setConstraints];
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.appDelegate.trainingTable.steps.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [CellManager getTrainingTableCell:self.appDelegate.trainingTable tableView:tableView idx:indexPath];
}

#pragma mark - Utilities

-(void) setControls {
    self.appDelegate.trainingTable.delegate = self;
    
    // set TableView
    [self setTableControls];

    // set ControlsView
    [self actionReset];

    [self setSliderValues];

    [self.view setNeedsUpdateConstraints]; // ???
}

-(void) setTableControls {
    // --- hide or show repeat slider
    // --- set slider's min/max values
    if ([self.appDelegate.trainingTable isKindOfClass:[TrainingTableEx class]]) {
        TrainingTableEx *tt = (TrainingTableEx*)self.appDelegate.trainingTable;
        
        self.sldrRepeats.minimumValue = 2;
        self.sldrRepeats.maximumValue = 15;
        
        self.sldrTimeBase.minimumValue = 2;
        self.sldrTimeBase.maximumValue = 12;
        
        self.sldrRepeats.value = tt.repeats;
    } else {
        self.sldrTimeBase.minimumValue = 5;
        self.sldrTimeBase.maximumValue = 20;
    }
    self.sldrTimeBase.value = self.appDelegate.trainingTable.timeBase;
    [self.tableView reloadData];
}

-(void) setLabels {
    [self updateTotalTime];
    [self updatePhaseTime];
    [self setNextPhase];
    [self setActivePhase];
}

-(void) setViewTableEnabled:(BOOL) enabled {
    if (self.viewTable.userInteractionEnabled == enabled) return;
    UIColor *c1 = (enabled) ? [UIColor blackColor] : [UIColor      grayColor];
    UIColor *c2 = (enabled) ? [UIColor  grayColor] : [UIColor lightGrayColor];

    if (enabled) {
        self.sldrTimeBase.tintColor = sliderColor;
        self.sldrRepeats.tintColor  = sliderColor;
        self.lblRepeats.textColor   = [UIColor blackColor];
        self.lblTimeBase.textColor  = [UIColor blackColor];
    } else {
        self.sldrTimeBase.tintColor = [UIColor lightGrayColor];
        self.sldrRepeats.tintColor  = [UIColor lightGrayColor];
        self.lblRepeats.textColor   = [UIColor lightGrayColor];
        self.lblTimeBase.textColor  = [UIColor lightGrayColor];
    }
    
    for (UIView *view in self.tableView.subviews) {
        for (id<TextColorDelegate> cell in view.subviews) {
            [cell setDurationColor:c1 andDescriptionColor:c2];
        }
    }
    
    for (UIBarButtonItem *item in self.navigationItem.rightBarButtonItems) {
        item.enabled = enabled;
    }
  
    [self.viewTable setUserInteractionEnabled:enabled];
}

#pragma mark - Timer View Protocol

- (void) updateTotalTime {
    self.lblTimeLeftValue.text = [NSString stringWithFormat:@"%@", [Tools secondsToTimeStr:[self.appDelegate.trainingTable getTotalTime]]];
}

- (void) updatePhaseTime {
    self.lblPhaseTime.text = [NSString stringWithFormat:@"%@", [Tools secondsToTimeStr:[self.appDelegate.trainingTable phaseDuration] withTens:YES]];
    
    float max = [self.appDelegate.trainingTable getInitialPhaseDuration:self.appDelegate.trainingTable.currentPhase forStep:self.appDelegate.trainingTable.currentStep];
    
    float val = (max - [self.appDelegate.trainingTable phaseDuration]) / max;
    
    [self.progress setProgress:val animated:NO];
    
}

- (void) setActivePhase {
    self.lblPhaseTitle.text = [self.appDelegate.trainingTable phaseDescription];
    [self.progress setProgress:0.0f animated:NO];
}

- (void) setNextPhase {
    float     nextPhaseDur = [self.appDelegate.trainingTable getNextPhaseDuration];
    NSString *nextPhaseDsc = [self.appDelegate.trainingTable getNextPhaseDescription];
    
    if (nextPhaseDur >= 0 && nextPhaseDsc != nil) {
        self.lblNextPhaseValue.text = [NSString stringWithFormat:@"%@ (%@)", nextPhaseDsc, [Tools secondsToTimeStr:nextPhaseDur]];
    } else {
        self.lblNextPhaseValue.text = NSLocalizedString(@"done", nil);
    }
}

#pragma mark - Layout

-(void) setConstraints {
        // clear current constraints
        [self clearAllConstraints];
    
        // set appropriate constraints
        UIInterfaceOrientation sbo = [[UIApplication sharedApplication] statusBarOrientation];
        if (sbo == UIInterfaceOrientationLandscapeLeft ||
            sbo == UIInterfaceOrientationLandscapeRight) {
            [self setLandscapeConstraints];
        } else {
            [self setPortraitConstraints];
        }
}

-(void) clearAllConstraints {
    self.lblPhaseTitle.translatesAutoresizingMaskIntoConstraints     = NO;
    self.lblPhaseTime.translatesAutoresizingMaskIntoConstraints      = NO;
    self.progress.translatesAutoresizingMaskIntoConstraints          = NO;
    self.lblNextPhaseTitle.translatesAutoresizingMaskIntoConstraints = NO;
    self.lblTimeLeftTitle.translatesAutoresizingMaskIntoConstraints  = NO;
    self.lblNextPhaseValue.translatesAutoresizingMaskIntoConstraints = NO;
    self.lblTimeLeftValue.translatesAutoresizingMaskIntoConstraints  = NO;
    self.lblStub.translatesAutoresizingMaskIntoConstraints           = NO;
    self.btnStart.translatesAutoresizingMaskIntoConstraints          = NO;
    self.btnSkip.translatesAutoresizingMaskIntoConstraints           = NO;

    self.tableView.translatesAutoresizingMaskIntoConstraints         = NO;
    self.lblTimeBase.translatesAutoresizingMaskIntoConstraints       = NO;
    self.sldrTimeBase.translatesAutoresizingMaskIntoConstraints      = NO;
    self.lblRepeats.translatesAutoresizingMaskIntoConstraints        = NO;
    self.sldrRepeats.translatesAutoresizingMaskIntoConstraints       = NO;
    
    [self.viewControls removeConstraints:self.viewControls.constraints];
    [self.viewTable    removeConstraints:self.viewTable.constraints];
}

-(void) setLandscapeConstraints {
    [self setViewConstraintsLandscape];
    [self setControlsConstraintsLandscape];
    [self setTableConstraintsLandscape];
}

-(void) setPortraitConstraints {
    [self setViewConstraintsPortrait];
    [self setControlsConstraintsPortrait];
    [self setTableConstraintsPortrait];
}

-(void) setTableConstraintsLandscape {
    NSDictionary *views = @{
                            @"table"   : self.tableView,
                            @"tblabel" : self.lblTimeBase,
                            @"tbslider": self.sldrTimeBase,
                            @"nrlabel" : self.lblRepeats,
                            @"nrslider": self.sldrRepeats,
                            };
    [self addSubViewConstraint:@"H:|-10-[table]-10-|"  toView:self.viewTable withDict:views];
    [self addSubViewConstraint:@"H:|-20-[tblabel]-20-|"  toView:self.viewTable withDict:views];
    [self addSubViewConstraint:@"H:|-40-[tbslider]-40-|" toView:self.viewTable withDict:views];
    
    [self addSubViewConstraint:@"H:|-20-[nrlabel]-20-|"  toView:self.viewTable withDict:views];
    [self addSubViewConstraint:@"H:|-40-[nrslider]-40-|" toView:self.viewTable withDict:views];
    
    if (self.hideRepeatsSlider) {
        [self addSubViewConstraint:@"V:|-20-[tblabel]-10-[tbslider]-50-[table]" toView:self.viewTable withDict:views];
        self.lblRepeats.hidden  = YES;
        self.sldrRepeats.hidden = YES;
    } else {
        [self addSubViewConstraint:@"V:|-20-[tblabel]-10-[tbslider]-20-[nrlabel]-10-[nrslider]-50-[table]" toView:self.viewTable withDict:views];
        self.lblRepeats.hidden  = NO;
        self.sldrRepeats.hidden = NO;
    }
    
    [self addSubViewConstraint:@"V:[tbslider(10)]" toView:self.viewTable withDict:views];
    [self addSubViewConstraint:@"V:[nrslider(10)]" toView:self.viewTable withDict:views];
    [self addSubViewConstraint:@"V:[table]-10-|" toView:self.viewTable withDict:views];
}

-(void) setTableConstraintsPortrait {
    NSDictionary *views = @{
                            @"table"   : self.tableView,
                            @"tblabel" : self.lblTimeBase,
                            @"tbslider": self.sldrTimeBase,
                            @"nrlabel" : self.lblRepeats,
                            @"nrslider": self.sldrRepeats,
                            };
    int w = (int)[[UIScreen mainScreen] bounds].size.width;
    NSString *str;

    [self addSubViewConstraint:@"V:[tbslider]-50-[table]-10-|" toView:self.viewTable withDict:views];

    [self addSubViewConstraint:@"V:|-20-[tblabel]" toView:self.viewTable withDict:views];
    [self addSubViewConstraint:@"V:[tblabel]-10-[tbslider]" toView:self.viewTable withDict:views];
    [self addSubViewConstraint:@"V:|-20-[nrlabel]" toView:self.viewTable withDict:views];
    [self addSubViewConstraint:@"V:[nrlabel]-10-[nrslider]" toView:self.viewTable withDict:views];

    if (self.hideRepeatsSlider) {
        self.lblRepeats.hidden = YES;
        self.sldrRepeats.hidden = YES;
        
        [self addSubViewConstraint:@"H:|-20-[tblabel]-20-|" toView:self.viewTable withDict:views];

        str = [NSString stringWithFormat:@"H:|-%d-[tbslider]-%d-|", w / 10, w / 10];
        [self addSubViewConstraint:str toView:self.viewTable withDict:views];
    } else {
        self.lblRepeats.hidden = NO;
        self.sldrRepeats.hidden = NO;

        str = [NSString stringWithFormat:@"H:|-20-[tblabel]-%d-|", w / 2];
        [self addSubViewConstraint:str toView:self.viewTable withDict:views];

        str = [NSString stringWithFormat:@"H:|-%d-[nrlabel]-20-|", w / 2];
        [self addSubViewConstraint:str toView:self.viewTable withDict:views];
    
        str = [NSString stringWithFormat:@"H:|-%d-[tbslider]-%d-|", w / 20, (w / 20) * 11];
        [self addSubViewConstraint:str toView:self.viewTable withDict:views];
    
        str = [NSString stringWithFormat:@"H:|-%d-[nrslider]-%d-|", w / 20 * 11, w / 20];
        [self addSubViewConstraint:str toView:self.viewTable withDict:views];
    
    }

    [self addSubViewConstraint:@"H:|-10-[table]-10-|" toView:self.viewTable withDict:views];
}

#define V_GAP 50
#define I_GAP 30
//#define H_GAP 20

-(void) setControlsConstraintsLandscape {
    
//[self.bottomView setNeedsUpdateConstraints];
    
    NSDictionary *views = @{@"title"   : self.lblPhaseTitle,
                            @"time"    : self.lblPhaseTime,
                            @"progress": self.progress,
                            @"nptitle" : self.lblNextPhaseTitle,
                            @"tltitle" : self.lblTimeLeftTitle,
                            @"npvalue" : self.lblNextPhaseValue,
                            @"tlvalue" : self.lblTimeLeftValue,
                            @"start"   : self.btnStart,
                            @"stop"    : self.btnSkip,
                            @"stub"    : self.lblStub
                           };
    NSString *str;
    str = [NSString stringWithFormat:
                                    @"V:|-%d-[title]-%d-[time]-%d-[progress]-%d-[nptitle]-%d-[tltitle]",
                                    V_GAP, I_GAP, I_GAP, I_GAP * 2, I_GAP];
    [self addSubViewConstraint:str                       toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"H:|-20-[title]-20-|"    toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"H:|-20-[time]-20-|"     toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"H:|-20-[nptitle]-100-|" toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"H:|-20-[tltitle]-100-|" toView:self.viewControls withDict:views];

    str = [NSString stringWithFormat:@"V:[progress]-%d-[npvalue]-%d-[tlvalue]", I_GAP * 2, I_GAP];
    [self addSubViewConstraint:str                       toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"H:[npvalue]-20-|"       toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"H:[tlvalue]-20-|"       toView:self.viewControls withDict:views];
    
    int n = (int)[[UIScreen mainScreen] bounds].size.width / 11;
    str = [NSString stringWithFormat:@"H:|-%d-[progress]-%d-|", n, n];
    [self addSubViewConstraint:str                       toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"V:[progress(3)]"        toView:self.viewControls withDict:views];

    str = [NSString stringWithFormat:@"V:[start]-%d-|", V_GAP];
    [self addSubViewConstraint:str                       toView:self.viewControls withDict:views];
    str = [NSString stringWithFormat:@"V:[stop]-%d-|", V_GAP];
    [self addSubViewConstraint:str                       toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"V:[stop(120)]"                 toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"V:[start(120)]"                toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"H:[start(120)][stub(50)][stop(120)]" toView:self.viewControls withDict:views];

    NSArray *constraints = [NSLayoutConstraint
                            constraintsWithVisualFormat:@"V:[view]-(<=1)-[stub]"
                                                options:NSLayoutFormatAlignAllCenterX
                                                metrics:nil
                            views: @{@"view":self.viewControls, @"stub":self.lblStub}];
    
    [self.viewControls addConstraints:constraints];
}

-(void) setControlsConstraintsPortrait {
    NSDictionary *views = @{@"title"   : self.lblPhaseTitle,
                            @"time"    : self.lblPhaseTime,
                            @"progress": self.progress,
                            @"nptitle" : self.lblNextPhaseTitle,
                            @"tltitle" : self.lblTimeLeftTitle,
                            @"npvalue" : self.lblNextPhaseValue,
                            @"tlvalue" : self.lblTimeLeftValue,
                            @"start"   : self.btnStart,
                            @"stop"    : self.btnSkip,
                            @"stub"    : self.lblStub
                            };
    
    int w = (int)[[UIScreen mainScreen] bounds].size.width;
    
    NSString *str;
    str = [NSString stringWithFormat:
           @"V:|-%d-[title]-%d-[time]-%d-[progress]-%d-[nptitle]-%d-[tltitle]",
           I_GAP, I_GAP, I_GAP, (int)(I_GAP * 1.5), I_GAP];
    [self addSubViewConstraint:str                       toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"H:|-50-[nptitle]"       toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"H:|-50-[tltitle]"       toView:self.viewControls withDict:views];
    
    str = [NSString stringWithFormat:@"V:[progress]-%d-[npvalue]-%d-[tlvalue]", (int)(I_GAP * 1.5), I_GAP];
    [self addSubViewConstraint:str                       toView:self.viewControls withDict:views];
    
    str = [NSString stringWithFormat:@"H:[npvalue]-%d-|", w / 3];
    [self addSubViewConstraint:str       toView:self.viewControls withDict:views];
    
    str = [NSString stringWithFormat:@"H:[tlvalue]-%d-|", w / 3];
    [self addSubViewConstraint:str       toView:self.viewControls withDict:views];
    
    str = [NSString stringWithFormat:@"H:|-80-[progress]-%d-|", w / 3 + 30];
    [self addSubViewConstraint:str       toView:self.viewControls withDict:views];
    
    str = [NSString stringWithFormat:@"H:|-50-[title]-%d-|", w / 3];
    [self addSubViewConstraint:str       toView:self.viewControls withDict:views];
    
    str = [NSString stringWithFormat:@"H:|-50-[time]-%d-|", w / 3];
    [self addSubViewConstraint:str       toView:self.viewControls withDict:views];
    
    
    [self addSubViewConstraint:@"H:[stop(120)]-50-|"                 toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"H:[start(120)]-50-|"                toView:self.viewControls withDict:views];
    [self addSubViewConstraint:@"V:[start(120)][stub(30)][stop(120)]" toView:self.viewControls withDict:views];
    
    NSArray *constraints = [NSLayoutConstraint
                            constraintsWithVisualFormat:@"H:[view]-(<=1)-[stub]"
                            options:NSLayoutFormatAlignAllCenterY
                            metrics:nil
                            views: @{@"view":self.viewControls, @"stub":self.lblStub}];
    
    [self.viewControls addConstraints:constraints];
}

-(void) setViewConstraintsLandscape {
    NSDictionary *views = @{@"ctlView":self.viewControls, @"tblView":self.viewTable, @"container":self.adContainer};
    int h = self.navigationController.navigationBar.frame.size.height;
    [self addSubViewConstraint:[NSString stringWithFormat:@"V:|-%d-[ctlView]", h + 30]
                        toView:self.view withDict:views];
    [self addSubViewConstraint:[NSString stringWithFormat:@"V:|-%d-[tblView]"   , h + 30]
                        toView:self.view withDict:views];

    [self addSubViewConstraint:@"V:[ctlView]-10-[container]-0-|"                   toView:self.view withDict:views];
    [self addSubViewConstraint:@"V:[tblView]-10-[container]-0-|"                   toView:self.view withDict:views];

    [self addSubViewConstraint:@"H:|-10-[ctlView]-10-[tblView]-10-|" toView:self.view withDict:views];
    [self addSubViewConstraint:@"H:[tblView(==ctlView)]"             toView:self.view withDict:views];
}

-(void) setViewConstraintsPortrait {
    NSDictionary *views = @{@"ctlView":self.viewControls, @"tblView":self.viewTable, @"container":self.adContainer};
    int h = self.navigationController.navigationBar.frame.size.height;
    
    [self addSubViewConstraint:[NSString stringWithFormat:@"V:|-%d-[ctlView]-10-[tblView]-10-[container]-0-|", h + 30]
                        toView:self.view
                      withDict:views];

    [self addSubViewConstraint:@"H:|-10-[ctlView]-10-|" toView:self.view withDict:views];
    [self addSubViewConstraint:@"H:|-10-[tblView]-10-|" toView:self.view withDict:views];
    [self addSubViewConstraint:[NSString stringWithFormat:@"V:[ctlView(%d)]", (int)([[UIScreen mainScreen] bounds].size.height)/3]
                        toView:self.view withDict:views];
}

#pragma mark - User Interface Functions

- (void) enableStartButton {
    self.btnStart.layer.borderColor = [green CGColor];
    [self.btnStart setTitleColor:green forState:UIControlStateNormal];
    self.btnStart.enabled = YES;
    [self.btnStart setTitle:NSLocalizedString(@"Start", nil) forState:UIControlStateNormal];
}

- (void) enableContinueButton {
    self.btnStart.layer.borderColor = [green CGColor];
    [self.btnStart setTitleColor:green forState:UIControlStateNormal];
    self.btnStart.enabled = YES;
    [self.btnStart setTitle:NSLocalizedString(@"Continue", nil) forState:UIControlStateNormal];
}

- (void) enablePauseButton {
    self.btnStart.layer.borderColor = [orange CGColor];
    [self.btnStart setTitleColor:orange forState:UIControlStateNormal];
    self.btnStart.enabled = YES;
    [self.btnStart setTitle:NSLocalizedString(@"Pause", nil) forState:UIControlStateNormal];
}

- (void) enableSkipButton {
    self.btnSkip.layer.borderColor = [blue CGColor];
    [self.btnSkip setTitleColor:blue forState:UIControlStateNormal];
    self.btnSkip.enabled = YES;
    [self.btnSkip setTitle:NSLocalizedString(@"Skip", nil) forState:UIControlStateNormal];
}

- (void) enableResetButton {
    self.btnSkip.layer.borderColor = [red CGColor];
    [self.btnSkip setTitleColor:red forState:UIControlStateNormal];
    self.btnSkip.enabled = YES;
    [self.btnSkip setTitle:NSLocalizedString(@"Reset", nil) forState:UIControlStateNormal];
}

- (void) disableResetButton {
    self.btnSkip.layer.borderColor = [lightGrey CGColor];
    [self.btnSkip setTitleColor:lightGrey forState:UIControlStateNormal];
    self.btnSkip.enabled = NO;
    [self.btnSkip setTitle:NSLocalizedString(@"Reset", nil) forState:UIControlStateNormal];
}

#pragma mark - Timer Functions

-(void) timerStart {
    if (self.appDelegate.timer.isPaused/*isValid]*/) {
        [self.appDelegate.timer start];
    } else {
        self.appDelegate.timer = [CSPausibleTimer timerWithTimeInterval:timerStep
                                                                 target:self
                                                               selector:@selector(timeStep)
                                                               userInfo:nil
                                                                repeats:YES];
        [self.appDelegate.timer start];
    }
}

- (void) timerPause {
//    NSLog(@"pause");
    [self.appDelegate.timer pause];
}

- (void) timerClear {
//    NSLog(@"clear");
    [self.appDelegate.timer invalidate];
     self.appDelegate.timer = nil;
}

- (void) timeStep {
    if (self.appDelegate.trainingTable.currentStep < 0) {
//        NSLog(@"timer step: done");
        [self trainingDone];
    } else {
//        NSLog(@"timer step: step");
        [self.appDelegate.trainingTable timeStep:timerStep];
        if (lastPhase != self.appDelegate.trainingTable.currentPhase &&
            [self.appDelegate.timer isActive]) {
            if (canPlay)
                [self playPhaseSound];
            else
                canPlay = TRUE;
        }
        lastPhase = self.appDelegate.trainingTable.currentPhase;
    }
}

#pragma mark - User Actions

-(void) trainingDone {
    NSDictionary *d = [self makeFlurryParams];
    if (d) [Flurry logEvent:@"training_done" withParameters:d];
    [self playDoneSound];
    [self actionReset];
    //    [GAEventLogger logEventAction:@"training" andLabel:@"done"];
}

-(void) actionStart {
//    self.btnTable.enabled = NO;
//    self.btnShare.enabled = NO;
//    [self.appDelegate disableTabBar];
    [self enablePauseButton];
    [self enableSkipButton];
    [self timerStart];
    [self setViewTableEnabled:NO];
//    [self.appDelegate.timer start];
//    [self.navigationItem setHidesBackButton:YES animated:YES];
    //    [GAEventLogger logEventAction:@"training" andLabel:@"start"];
}

-(void) actionPause {
    [self enableContinueButton];
    [self enableResetButton];
    [self timerPause];
    //    [GAEventLogger logEventAction:@"training" andLabel:@"pause"];
}

-(void) actionSkip {
//    NSLog(@"skip");
    canPlay = NO;
    [self timerClear];
    [self.appDelegate.trainingTable nextPhase:YES];
    if (self.appDelegate.trainingTable.currentPhase >= 0 && self.appDelegate.trainingTable.currentStep >= 0) {
        [self setLabels];
        [self timerStart];
    } else {
        [self actionReset];
    }
    //    [GAEventLogger logEventAction:@"training" andLabel:@"skip"];
}

-(void) actionReset {
    //    self.btnTable.enabled = YES;
    //    self.btnShare.enabled = YES;
    canPlay = NO;
    [self enableStartButton];
    [self disableResetButton];
    [self timerClear];
    [self.appDelegate.trainingTable reset];
    [self.appDelegate.trainingTable nextPhase:YES];
    [self setLabels];
    [self setViewTableEnabled:YES];

    //    if ([self isKindOfClass:[TTTimerViewController class]]) {
    //        [self.navigationItem setHidesBackButton:NO animated:YES];
    //    } else {
    //        [self.appDelegate enableTabBar];
    //        self.navigationItem.backBarButtonItem.enabled = NO;
    //    }
    lastPhase = -1;
    //    [GAEventLogger logEventAction:@"training" andLabel:@"reset"];
}


-(void) btnStartPressed:(UIButton*)sender {
    [self playClickSound];
    NSDictionary *d = [self makeFlurryParams];
    if (self.appDelegate.timer.isValid) {
        if (d) [Flurry logEvent:@"training_pause" withParameters:d];
        [self actionPause];
    } else {
        if (d) [Flurry logEvent:@"training_start" withParameters:d];
        [self actionStart];
    }
}

-(void) btnSkipPressed:(UIButton*)sender {
    [self playClickSound];
    NSDictionary *d = [self makeFlurryParams];
    if (self.appDelegate.timer.isValid) {
        if (d) [Flurry logEvent:@"training_skip" withParameters:d];
        [self actionSkip];
    } else {
        if (d) [Flurry logEvent:@"training_reset" withParameters:d];
        [self actionReset];
    }
}

#pragma mark - Sliders

-(void) sliderTimeBaseAction:(id)sender {
    UISlider *slider = (UISlider*)sender;
    [slider setValue:[self roundSliderValue:slider.value] animated:YES];
    [self.appDelegate setTimeBase:(int)slider.value];
    [self.appDelegate.trainingTable reinit:(int)slider.value];
    [self.tableView reloadData];
    [self actionReset];
    [self setSliderValues];
}

-(void) sliderRepeatsAction:(id)sender {
    UISlider *slider = (UISlider*)sender;
    [slider setValue:[self roundSliderValue:slider.value] animated:YES];
    [self.appDelegate setRepeats:(int)slider.value];

    TrainingTableEx *tt = (TrainingTableEx*)self.appDelegate.trainingTable;
    
    [tt reinitWithRepeats:(int)slider.value];
    [self.tableView reloadData];
    [self actionReset];
    [self setSliderValues];
}

-(int) roundSliderValue:(float)x {
    if (x - (int)x > 0.5) return ((int)x + 1);
    else return (int) x;
}

-(void) setSliderValues {
    self.lblTimeBase.text = [NSString stringWithFormat:@"%@: %d %@",
                             NSLocalizedString(@"Time Base", nil),
                             (int)self.sldrTimeBase.value,
                             NSLocalizedString(@"sec", nil)
                             ];
    self.lblRepeats.text = [NSString stringWithFormat:@"%@: %d",
                            NSLocalizedString(@"Repeats", nil),
                            (int)self.sldrRepeats.value
                            ];
}

#pragma mark - Sound Functions

- (void) playSound:(NSString *)name withExtension:(NSString *)extension
{
    if (self.appDelegate.useSound != 0) {
        NSURL* soundUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:name ofType:extension]];
        self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
        self.player.delegate = self;
        self.player.volume = 1.0;
        [self.player play];
    }
}

- (void) playClickSound {
    [self playSound:@"click2" withExtension:@"wav"];
}

- (void) playPhaseSound {
    [self playSound:@"ding" withExtension:@"wav"];
}

- (void) playDoneSound {
    [self playPhaseSound];
    [NSTimer scheduledTimerWithTimeInterval:0.15f
                                     target:self
                                   selector:@selector(playPhaseSound)
                                   userInfo:nil
                                    repeats:NO];
    [NSTimer scheduledTimerWithTimeInterval:0.3f
                                     target:self
                                   selector:@selector(playPhaseSound)
                                   userInfo:nil
                                    repeats:NO];
}


-(NSDictionary*) makeFlurryParams {
    if (!self.appDelegate.trainingTable) return nil;
    NSDictionary *params;
    if ([self.appDelegate.trainingTable isKindOfClass:[TrainingTableEx class]]) {
        TrainingTableEx *tt = (TrainingTableEx*)self.appDelegate.trainingTable;
//        NSLog(@"name: %@", [tt getName]);
//        NSLog(@"base: %@", [NSNumber numberWithInt:tt.timeBase]);
//        NSLog(@"rpts: %@", [NSNumber numberWithInt:tt.repeats]);
        params = @{
                   @"table"   : [tt getName],
                   @"tbase"   : [NSNumber numberWithInt:tt.timeBase],
                   @"repeats" : [NSNumber numberWithInt:tt.repeats],
                  };
    } else {
//        NSLog(@"name: %@", [self.appDelegate.trainingTable getName]);
//        NSLog(@"base: %@", [NSNumber numberWithInt:self.appDelegate.trainingTable.timeBase]);
        params = @{
                   @"table"   : [self.appDelegate.trainingTable getName],
                   @"tbase"   : [NSNumber numberWithInt:self.appDelegate.trainingTable.timeBase],
                  };
    }
    return params;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
