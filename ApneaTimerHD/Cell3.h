//
//  Cell3.h
//  ApneaTimerHD
//
//  Created by Mikhail on 12.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "CellCommon.h"

static NSString * const idCell3 = @"Cell3";

@interface Cell3 : CellCommon

@property (nonatomic, strong) UILabel *lblDuration1;
@property (nonatomic, strong) UILabel *lblDuration2;
@property (nonatomic, strong) UILabel *lblDuration3;
@property (nonatomic, strong) UILabel *lblDescription1;
@property (nonatomic, strong) UILabel *lblDescription2;
@property (nonatomic, strong) UILabel *lblDescription3;

@end
