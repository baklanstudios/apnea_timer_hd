//
//  SettingsViewController.m
//  ApneaTimerHD
//
//  Created by Mikhail on 17.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "SettingsViewController.h"
#import "AppDelegate.h"
#import "MainViewControllerPrototype.h"

#import "Flurry.h"

#define kRemoveAdsProductIdentifier @"ws.slink.apneatimer.hd.removeads"

@interface SettingsViewController()

@property (weak, nonatomic)   AppDelegate *appDelegate;

@property (strong, nonatomic) UILabel  *lblStub;
@property (strong, nonatomic) UILabel  *lblSound;
@property (strong, nonatomic) UISwitch *swtSound;
@property (strong, nonatomic) UIButton *btnClose;

@property (strong, nonatomic) UIButton *btnPurchase;
@property (strong, nonatomic) UIButton *btnRestore;

@end


@implementation SettingsViewController

-(SettingsViewController*) init {
    self = [super init];
    if (self) {
        self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [self createControls];
    }
    return self;
}

-(void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.view.superview.bounds = CGRectMake(0, 0, 400, 400);
}

-(void) createControls {
    self.lblSound = [[UILabel alloc] init];
    self.lblSound.text = NSLocalizedString(@"Sounds", nil);
//    self.lblSound.font = [UIFont systemFontOfSize:22];
    self.lblSound.textAlignment = NSTextAlignmentLeft;
//    self.lblSound.backgroundColor = [UIColor greenColor];
    [self.view addSubview:self.lblSound];

    self.lblStub = [[UILabel alloc] init];
//    self.lblStub.backgroundColor = [UIColor grayColor];
    [self.view addSubview:self.lblStub];
    
    self.swtSound    = [[UISwitch alloc] init];
    self.swtSound.on = self.appDelegate.useSound;
    [self.swtSound addTarget:self action:@selector(changeUseSound:) forControlEvents:UIControlEventValueChanged];
//    self.swtSound.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.swtSound];

    
    self.btnPurchase = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.btnPurchase setTitle:NSLocalizedString(@"remove ads", nil) forState:UIControlStateNormal];
    [[self.btnPurchase layer] setBorderWidth:1.0f];
    [[self.btnPurchase layer] setCornerRadius:5.0f];
    [[self.btnPurchase layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [self.btnPurchase addTarget:self
                      action:@selector(btnPurchasePressed)
            forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.btnPurchase];

    
    self.btnRestore = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.btnRestore setTitle:NSLocalizedString(@"restore purchase", nil) forState:UIControlStateNormal];
    [[self.btnRestore layer] setBorderWidth:1.0f];
    [[self.btnRestore layer] setCornerRadius:5.0f];
    [[self.btnRestore layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [self.btnRestore addTarget:self
                      action:@selector(btnRestorePressed)
            forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.btnRestore];

    
    self.btnClose = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.btnClose setTitle:NSLocalizedString(@"done", nil) forState:UIControlStateNormal];
    [[self.btnClose layer] setBorderWidth:1.0f];
    [[self.btnClose layer] setCornerRadius:5.0f];
    [[self.btnClose layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [self.btnClose addTarget:self
                      action:@selector(btnClosePressed:)
            forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.btnClose];
    
    [self setConstraints];
}

-(void) viewDidLoad {
    self.view.backgroundColor = [UIColor whiteColor];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)willAnimateRotationToInterfaceOrientation:
(UIInterfaceOrientation)toInterfaceOrientation
                                         duration:(NSTimeInterval)duration {
    [self.view setNeedsUpdateConstraints];
}

-(void) updateViewConstraints {
    [super updateViewConstraints];
    [self setConstraints];
}


-(void) setConstraints {
    // clear current constraints
    [self clearConstraints];

    // set appropriate constraints
//    UIInterfaceOrientation sbo = [[UIApplication sharedApplication] statusBarOrientation];
//    if (sbo == UIInterfaceOrientationLandscapeLeft ||
//        sbo == UIInterfaceOrientationLandscapeRight) {
//        [self setLandscapeConstraints];
//    } else {
//        [self setPortraitConstraints];
//    }
    [self setControlsConstraints];
}


-(void) clearConstraints {
    self.lblSound.translatesAutoresizingMaskIntoConstraints = NO;
    self.swtSound.translatesAutoresizingMaskIntoConstraints = NO;
    self.btnClose.translatesAutoresizingMaskIntoConstraints = NO;
    self.btnPurchase.translatesAutoresizingMaskIntoConstraints = NO;
    self.btnRestore.translatesAutoresizingMaskIntoConstraints = NO;
    self.lblStub.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view removeConstraints:self.view.constraints];
}

-(void) setControlsConstraints {
    NSDictionary *views = @{
                              @"sound"   : self.lblSound,
                              @"sswt"    : self.swtSound,
                              @"close"   : self.btnClose,
                              @"view"    : self.view,
                              @"stub"    : self.lblStub,
                              @"purchase": self.btnPurchase,
                              @"restore" : self.btnRestore,
                           };
    [self addSubViewConstraint:@"V:|-50-[sound(==30)]"                   toView:self.view withDict:views];
    [self addSubViewConstraint:@"V:|-50-[sswt(==30)]"                    toView:self.view withDict:views];
    [self addSubViewConstraint:@"V:[close(==40)]-50-|"                   toView:self.view withDict:views];

    if (self.appDelegate.purchased) {
        self.btnPurchase.hidden = YES;
        self.btnRestore.hidden  = YES;
        [self addSubViewConstraint:@"V:[sswt]-50-[purchase(==0)]-25-[restore(==0)]" toView:self.view withDict:views];
    } else {
        self.btnPurchase.hidden = NO;
        self.btnRestore.hidden  = NO;
        [self addSubViewConstraint:@"V:[sswt]-50-[purchase(==40)]-25-[restore(==40)]" toView:self.view withDict:views];
    }

    [self addSubViewConstraint:@"H:[purchase(==250)]" toView:self.view withDict:views];
    [self addSubViewConstraint:@"H:[restore(==250)]" toView:self.view withDict:views];

    [self addSubViewConstraint:@"H:|-90-[sound(==100)][stub][sswt]-90-|" toView:self.view withDict:views];
    [self addSubViewConstraint:@"H:[close(==150)]"                       toView:self.view withDict:views];

    NSArray *constraints;
    
    constraints = [NSLayoutConstraint
                             constraintsWithVisualFormat:@"V:[view]-(<=1)-[close]"
                             options:NSLayoutFormatAlignAllCenterX
                             metrics:nil
                             views: views];
    [self.view addConstraints:constraints];
    
    constraints = [NSLayoutConstraint
                   constraintsWithVisualFormat:@"V:[view]-(<=1)-[purchase]"
                   options:NSLayoutFormatAlignAllCenterX
                   metrics:nil
                   views: views];
    [self.view addConstraints:constraints];
    
    constraints = [NSLayoutConstraint
                   constraintsWithVisualFormat:@"V:[view]-(<=1)-[restore]"
                   options:NSLayoutFormatAlignAllCenterX
                   metrics:nil
                   views: views];
    [self.view addConstraints:constraints];

}

-(void) addSubViewConstraint:(NSString*)str
                      toView:target
                    withDict:dict {
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:str options:0 metrics:nil views:dict];
    [target addConstraints:constraints];
}

-(void) btnClosePressed:(id)sender {
    [self.appDelegate setNeedsSave:YES];
    [self clearConstraints];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) changeUseSound:(id)sender {
    self.appDelegate.useSound = ((UISwitch*)sender).on;
}

# pragma mark - In-App purchase
-(void) btnPurchasePressed {
    if([SKPaymentQueue canMakePayments]){
        NSLog(@"User can make payments");
        SKProductsRequest *productsRequest =
        [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:kRemoveAdsProductIdentifier]];
        productsRequest.delegate = self;
        [productsRequest start];
    }
    else{
        NSLog(@"User cannot make payments due to parental controls");
        //this is called the user cannot make payments, most likely due to parental controls
    }
    
//    [self.appDelegate setPurchased:YES];
//    [self.appDelegate setNeedsSave:YES];
//    [self.delegate hideBanner];
//    [self.view setNeedsUpdateConstraints];
//    NSLog(@"purchase");
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    SKProduct *validProduct = nil;
    
    for (NSString *invalidIdentifier in response.invalidProductIdentifiers) {
        NSLog(@"Invalid ID requested: %@", invalidIdentifier);
    }
    
    int count = (int)[response.products count];
    if(count > 0){
        validProduct = [response.products objectAtIndex:0];
        NSLog(@"Products Available!");
        [self purchase:validProduct];
    }
    else if(!validProduct){
        NSLog(@"No products available");
        //this is called if your product id is not valid, this shouldn't be called unless that happens.
    }
}

- (IBAction)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    for(SKPaymentTransaction *transaction in transactions){
        if (transaction.transactionState == SKPaymentTransactionStatePurchasing){
            NSLog(@"Transaction state -> Purchasing");
            //called when the user is in the process of purchasing, do not add any of your own code here.
        } else if (transaction.transactionState == SKPaymentTransactionStatePurchased) {
            //this is called when the user has successfully purchased the package (Cha-Ching!)
            [self removeAds]; //you can add your code for what you want to happen when the user buys the purchase here, for this tutorial we use removing ads
            [Flurry logEvent:@"purchased_ads_removal"];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            NSLog(@"Transaction state -> Purchased");
        } else if (transaction.transactionState == SKPaymentTransactionStateRestored) {
            NSLog(@"Transaction state -> Restored");
            //add the same code as you did from SKPaymentTransactionStatePurchased here
            [Flurry logEvent:@"restored_ads_removal"];
//            [self.activity hide:NSLocalizedString(@"purchase successfully restored", nil)];
            [self removeAds];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        } else if (transaction.transactionState == SKPaymentTransactionStateFailed) {
            //called when the transaction does not finnish
            if(transaction.error.code != SKErrorPaymentCancelled){
                NSLog(@"Transaction state -> Cancelled");
                //the user cancelled the payment ;(
            }
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        }
    }
}

-(void) btnRestorePressed {
    NSLog(@"Trying to restore a purchase");
//    [self.activity show];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"received restored transactions: %i", (int)queue.transactions.count);
    for (SKPaymentTransaction *transaction in queue.transactions)
    {
        if(SKPaymentTransactionStateRestored){
            NSLog(@"Transaction state -> Restored");
            //called when the user successfully restores a purchase
            //[self.activity hide:NSLocalizedString(@"purchase successfully restored", nil)];
            [self removeAds];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
        
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    NSLog(@"Error restoring purchases: %@", error);
    /*
    [self.activity hide:
     [NSString stringWithFormat:@"%@:\n\n%@",
      NSLocalizedString(@"failed to restore a purchase", nil),
      [error localizedDescription]
      ]];
    */
}

- (void) removeAds {
    [self.appDelegate setPurchased:YES];
    [self.appDelegate setNeedsSave:YES];
    [self.delegate hideBanner];
    [self.view setNeedsUpdateConstraints];
    //    self.appDelegate.purchased = TRUE;
    //    self.appDelegate.bannerDelegate.bannerIsVisible = NO;
    ////    self.btnRemoveAds.hidden = YES;
    ////    self.btnRemoveAds.enabled = NO;
    //    self.btnRestorePurchase.hidden = YES;
    //    self.btnRestorePurchase.enabled = NO;
    //    [self.appDelegate hideAdBanner];
    //    [self.appDelegate saveSettings];
}



@end
