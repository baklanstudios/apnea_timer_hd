//
//  MainViewControllerPrototype.m
//  ApneaTimerHD
//
//  Created by Mikhail on 08.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <Social/Social.h>
#import <Accounts/Accounts.h>

#import "MainViewControllerPrototype.h"
#import "MFSideMenu.h"
#import "AdBannerDelegate.h"
#import "AppDelegate.h"
#import "SettingsViewController.h"

#import "Flurry.h"

@interface MainViewControllerPrototype ()
{
}

@end

@implementation MainViewControllerPrototype

#pragma mark - View Controller 

- (void)viewDidLoad {
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [self setupMenuBarButtonItems];
    
        // iAd container
        self.adContainer = [[UIView alloc] init];
        [self.view addSubview:self.adContainer];
        self.bannerDelegate = self.appDelegate.bannerDelegate;
    
    if (self.appDelegate.purchased) {
        self.bannerDelegate.bannerVisible = NO;
    }

    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Constraints

-(void) updateViewConstraints {
//    if (!self.appDelegate.purchased) {
        [self setBannerConstraints];
//    }
    [super updateViewConstraints];
}

-(void) setBannerConstraints {
    [self.adContainer addSubview:self.bannerDelegate.banner];

    self.adContainer.translatesAutoresizingMaskIntoConstraints = NO;
    self.bannerDelegate.banner.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.bannerDelegate.banner  removeConstraints:self.bannerDelegate.banner.constraints];
    [self.adContainer  removeConstraints:self.adContainer.constraints];
    [self.view  removeConstraints:self.view.constraints];
    
    NSDictionary *views = @{@"container":self.adContainer, @"banner":self.bannerDelegate.banner};
    
    [self addSubViewConstraint:@"H:|-0-[container]-0-|" toView:self.view withDict:views];
    [self addSubViewConstraint:@"V:[container]-0-|"     toView:self.view withDict:views];

    [self addSubViewConstraint:@"H:|-0-[banner]-0-|" toView:self.adContainer withDict:views];
    [self addSubViewConstraint:@"V:|-0-[banner]-0-|" toView:self.adContainer withDict:views];

    if (self.bannerDelegate.bannerVisible)
        [self addSubViewConstraint:@"V:[container(66)]" toView:self.view withDict:views];
    else
        [self addSubViewConstraint:@"V:[container(0)]" toView:self.view withDict:views];
}

-(void) addSubViewConstraint:(NSString*)str
                      toView:target
                    withDict:dict {
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:str options:0 metrics:nil views:dict];
    [target addConstraints:constraints];
}

-(void) hideBanner {
    self.bannerDelegate.bannerVisible = NO;
    [self.view setNeedsUpdateConstraints];
}

-(void) showBanner {
    self.bannerDelegate.bannerVisible = YES;
    [self.view setNeedsUpdateConstraints];
}


#pragma mark - UIBarButtonItems
- (void)setupMenuBarButtonItems {

    // set left buttons
    UIBarButtonItem *bookmarksBarButtonItem =
    [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks
                                                 target:self
                                                 action:@selector(menuAction)];
    self.navigationItem.leftBarButtonItems =
        [[NSArray alloc] initWithObjects:bookmarksBarButtonItem, nil];

    // set right buttons
    UIBarButtonItem *actionBarButtonItem =
    [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                 target:self
                                                 action:@selector(shareAction:)];

    UIBarButtonItem *settingsButton =
    [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"gear32.png"]
                                    style:(UIBarButtonItemStylePlain)
                                   target:self
                                   action:@selector(settingsAction)];
    
    self.navigationItem.rightBarButtonItems =
    [[NSArray alloc] initWithObjects:actionBarButtonItem, settingsButton, nil];
}

-(void) setButtonsEnabled:(BOOL)enabled {
    for (UIBarButtonItem *item in self.navigationItem.leftBarButtonItems) {
        item.enabled = enabled;
    }
    for (UIBarButtonItem *item in self.navigationItem.rightBarButtonItems) {
        item.enabled = enabled;
    }
}

#pragma mark - User Actions

- (void)cancelAction {
    UINavigationController *navController = self.appDelegate.container.centerViewController;
    [navController popViewControllerAnimated:YES];
}

- (void)menuAction {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

-(void)settingsAction {
    SettingsViewController *vc = [[SettingsViewController alloc] init];
    vc.modalPresentationStyle  = UIModalPresentationFormSheet;
    vc.modalTransitionStyle    = UIModalTransitionStyleFlipHorizontal;
    vc.delegate                = self;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)shareAction:(id)sender {
    [self setButtonsEnabled:NO];
    [self actionShareButtonPressed:sender];
}


#pragma mark - Social Sharing

- (void) actionShareButtonPressed:(id)sender {
    if ([self.appDelegate.timer isActive]) return;
    UIActionSheet *sheet = [[UIActionSheet alloc]
                            initWithTitle:nil
                            delegate:self
                            cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                            destructiveButtonTitle:nil
                            otherButtonTitles:
                            NSLocalizedString(@"Share on Facebook", nil),
                            NSLocalizedString(@"Share on Twitter", nil),
                            nil];
    sheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    sheet.tag = 1;
//    [sheet showInView:self.view];
    [sheet showFromBarButtonItem:sender animated:NO];
}


-(void) actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex {
//    _actionSheetShown = NO;
    [self setButtonsEnabled:YES];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            //if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        {
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [controller addImage:[self getShareImage]];
            [controller setInitialText:[self getShareText]];
            [self presentViewController:controller animated:YES completion:nil];
            [Flurry logEvent:@"share_facebook"];
            //            [GAEventLogger logEventAction:@"social" andLabel:@"facebook"];
        }
            break;
        case 1:
            //if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet setInitialText:[self getShareText]];
            [self presentViewController:tweetSheet animated:YES completion:nil];
            [Flurry logEvent:@"share_twitter"];
            //            [GAEventLogger logEventAction:@"social" andLabel:@"twitter"];
        }
            break;
        default:
            break;
    }
}

-(NSString*) getShareText {
    NSString *appLink = @"https://itunes.apple.com/app/id958951681";
    return [NSString stringWithFormat:@"I'm doing my APNEA training with ApneaTimerHD (%@)", appLink];
}
-(UIImage*) getShareImage {
    return [UIImage imageNamed:@"apnea_poster.jpg"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
