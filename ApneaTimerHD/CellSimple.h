//
//  CellSimple.h
//  ApneaTimerHD
//
//  Created by Mikhail on 12.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellCommon.h"

static NSString * const idCellSimple = @"CellSimple";

@interface CellSimple : CellCommon

@property (nonatomic, strong) UILabel *lblDuration;
@property (nonatomic, strong) UILabel *lblDescription;

@end
