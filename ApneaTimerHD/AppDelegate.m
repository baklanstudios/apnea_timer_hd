//
//  AppDelegate.m
//  ApneaTimerHD
//
//  Created by Mikhail on 08.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "AppDelegate.h"
#import "MFSideMenuContainerViewController.h"
#import "MenuViewController.h"
#import "MainViewControllerSplash.h"
#import "MainViewControllerTimer.h"
#import "MainViewControllerPrototype.h"
#import "TableSelectionDelegate.h"
#import "AdBannerDelegate.h"

#import "CO2TableClass.h"
#import "O2TableClass.h"
#import "TriTableClass.h"
#import "SqTableClass.h"

#import "Flurry.h"

#import "NSTimer+Block.h"

@interface AppDelegate ()

@property (strong, nonatomic) NSArray *tables;
@property (strong, nonatomic) NSArray *tableNames;
@property (strong, nonatomic) NSArray *tableTitles;

@end

@implementation AppDelegate

#pragma mark - Defines
#define MENU_WIDTH 350

#pragma mark - Application Delegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [Flurry startSession:@"T88HGJMVZB25XVP7ZCXB"];
    //    [Flurry setLogLevel:FlurryLogLevelAll];
    //    [Flurry logAllPageViews:tabController];
    
    self.needsSave = NO;
    self.tableIndex = -1;
    
    [self loadSettings];
    [self initTrainingTables];
    
    self.bannerDelegate = [[AdBannerDelegate alloc] init];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    self.container = (MFSideMenuContainerViewController *)self.window.rootViewController;
    
    UINavigationController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"menuController"];
    UINavigationController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainController"];

    self.timerView  = [storyboard instantiateViewControllerWithIdentifier:@"timerViewController"];
//    self.splashView = (MainViewControllerSplash*)[mainViewController presentingViewController];
    
    [self.container setLeftMenuViewController:menuViewController];
    [self.container setCenterViewController:mainViewController];
    [self.container setLeftMenuWidth:MENU_WIDTH];

    MenuViewController *menu = [[menuViewController viewControllers] firstObject];
    [menu setDataTable:self.tableNames];
     menu.delegate = self;
    
    /////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //[self selectedTableIndex:0];

    // try to save settings if needed once in 5 sec
    [NSTimer scheduledTimerWithTimeInterval:5
        repeats:YES
          block:(
            ^{
               if(self.needsSave) {
                   [self saveSettings];
                   [self setNeedsSave:NO];
               }
             })];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Training Tables

-(void) initTrainingTables {
    NSString *breathing = NSLocalizedString(@"breathing", nil);
    self.tableNames = @[
                            NSLocalizedString(@"CO2 Training", nil),
                            NSLocalizedString(@"O2 Training", nil),
                            [NSString stringWithFormat:@"\u25B3 %@ (A)", breathing],
                            [NSString stringWithFormat:@"\u25B3 %@ (B)", breathing],
                            [NSString stringWithFormat:@"\u25A2 %@ (A)", breathing],
                            [NSString stringWithFormat:@"\u25A2 %@ (B)", breathing],
                            [NSString stringWithFormat:@"\u25A2 %@ (C)", breathing]
                      ];
    self.tableTitles = @[
                        NSLocalizedString(@"CO2 Training", nil),
                        NSLocalizedString(@"O2 Training", nil),
                        NSLocalizedString(@"Triangular Breathing (A)", nil),
                        NSLocalizedString(@"Triangular Breathing (B)", nil),
                        NSLocalizedString(@"Square Breathing (A)", nil),
                        NSLocalizedString(@"Square Breathing (B)", nil),
                        NSLocalizedString(@"Square Breathing (C)", nil)
                        ];
    self.tables = @[
               [[CO2TableClass alloc] initWithTimeBase:self.timeBaseCO2],
               
               [[O2TableClass alloc] initWithTimeBase:self.timeBaseO2],
               
               [[TriTableClass alloc] initTableA:@"TriA"//@"\u25B3 breathing (A)"
                                    withTimeBase:self.timeBaseTriA
                                       andRepeat:self.repeatsTriA],
               
               [[TriTableClass alloc] initTableB:@"TriB"//@"\u25B3 breathing (B)"
                                    withTimeBase:self.timeBaseTriB
                                       andRepeat:self.repeatsTriB],
               
               [[SqTableClass  alloc] initTableA:@"SqrA"//@"\u25A2 breathing (A)"
                                    withTimeBase:self.timeBaseSqA
                                       andRepeat:self.repeatsSqA],
               
               [[SqTableClass  alloc] initTableB:@"SqrB"//@"\u25A2 breathing (B)"
                                    withTimeBase:self.timeBaseSqB
                                       andRepeat:self.repeatsSqB],
               
               [[SqTableClass  alloc] initTableC:@"SqrC"//@"\u25A2 breathing (C)"
                                    withTimeBase:self.timeBaseSqC
                                       andRepeat:self.repeatsSqC]
               ];
}

#pragma mark - Table Selection Delegate

-(void)selectedTableName:(NSString*)tableName{
    NSLog(@"selected table name: %@", tableName);
}

-(void)selectedTableIndex:(int)index {

    if (self.tableIndex == index) return;
    
    self.tableIndex      = index;
    self.trainingTable   = self.tables[index];
    self.timerView.title = self.tableTitles[index];

    if ([[self.trainingTable getName] isEqualToString:@"CO2"] || [[self.trainingTable getName] isEqualToString:@"O2"]) {
        self.timerView.hideRepeatsSlider = YES;
    } else {
        self.timerView.hideRepeatsSlider = NO;
    }
    [self.timerView setControls];
    
    UINavigationController *navController = self.container.centerViewController;
    if ([[navController topViewController] isKindOfClass:[MainViewControllerTimer class]]) {
        return;
    }
    [navController popViewControllerAnimated:YES];
    [navController pushViewController:self.timerView animated:YES];
    
    /*
    // custom animation for loading timerView
    [navController popViewControllerAnimated:NO];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.50];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:navController.view cache:YES];
    [navController pushViewController:self.timerView animated:NO];
    [UIView commitAnimations];
    */
}

-(void) setTimeBase:(int)val {
    switch (self.tableIndex) {
        case 0:
            self.timeBaseCO2 = val;
            break;
        case 1:
            self.timeBaseO2 = val;
            break;
        case 2:
            self.timeBaseTriA = val;
            break;
        case 3:
            self.timeBaseTriB = val;
            break;
        case 4:
            self.timeBaseSqA = val;
            break;
        case 5:
            self.timeBaseSqB = val;
            break;
        case 6:
            self.timeBaseSqC = val;
            break;
        default:
            break;
    }
    [self setNeedsSave:YES];
}

-(void) setRepeats:(int)val {
    switch (self.tableIndex) {
        case 2:
            self.repeatsTriA = val;
            break;
        case 3:
            self.repeatsTriB = val;
            break;
        case 4:
            self.repeatsSqA = val;
            break;
        case 5:
            self.repeatsSqB = val;
            break;
        case 6:
            self.repeatsSqC = val;
            break;
        default:
            break;
    }
    [self setNeedsSave:YES];
}

#pragma mark - Saving/Loading User Settings

- (void)saveSettings {
    [[NSUserDefaults standardUserDefaults] setInteger:(self.purchased) ? 1 : 0
                                               forKey: @"adsPurchased"];
    [[NSUserDefaults standardUserDefaults] setInteger:(self.useSound) ? 1 : 0
                                               forKey: @"useSound"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseO2
                                               forKey: @"timeBaseO2"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseCO2
                                               forKey: @"timeBaseCO2"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseTriA
                                               forKey: @"timeBaseTriA"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.repeatsTriA
                                               forKey: @"repeatsTriA"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseTriB
                                               forKey: @"timeBaseTriB"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.repeatsTriB
                                               forKey: @"repeatsTriB"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseSqA
                                               forKey: @"timeBaseSqA"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.repeatsSqA
                                               forKey: @"repeatsSqA"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseSqB
                                               forKey: @"timeBaseSqB"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.repeatsSqB
                                               forKey: @"repeatsSqB"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseSqC
                                               forKey: @"timeBaseSqC"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.repeatsSqC
                                               forKey: @"repeatsSqC"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)loadSettings {
    bool doSave = false;
    int val;
    
    if ([self loadIntParam:@"useSound"     into:&val notFound: 1]) doSave=true; self.useSound     = (val == 1) ? YES : NO;
    //    NSLog(@"useSound: %d", self.useSound);
    
    if ([self loadIntParam:@"adsPurchased" into:&val notFound: 0]) doSave=true; self.purchased    = (val == 1) ? YES : NO;
    //    NSLog(@"adsPurchased: %d", self.purchased);
    
    if ([self loadIntParam:@"timeBaseO2"   into:&val notFound:10]) doSave=true; self.timeBaseO2   = val;
    //    NSLog(@"timeBaseO2: %d", self.timeBaseO2);
    if ([self loadIntParam:@"timeBaseCO2"  into:&val notFound:10]) doSave=true; self.timeBaseCO2  = val;
    //    NSLog(@"timeBaseCO2: %d", self.timeBaseCO2);
    
    if ([self loadIntParam:@"timeBaseTriA" into:&val notFound: 4]) doSave=true; self.timeBaseTriA = val;
    //    NSLog(@"timeBaseTriA: %d", self.timeBaseTriA);
    if ([self loadIntParam:@"repeatsTriA"  into:&val notFound:10]) doSave=true; self.repeatsTriA  = val;
    //    NSLog(@"repeatsTriA: %d", self.repeatsTriA);
    
    if ([self loadIntParam:@"timeBaseTriB" into:&val notFound: 4]) doSave=true; self.timeBaseTriB = val;
    //    NSLog(@"timeBaseTriB: %d", self.timeBaseTriB);
    if ([self loadIntParam:@"repeatsTriB"  into:&val notFound: 6]) doSave=true; self.repeatsTriB  = val;
    //    NSLog(@"repeatsTriB: %d", self.repeatsTriB);
    
    if ([self loadIntParam:@"timeBaseSqA"  into:&val notFound: 4]) doSave=true; self.timeBaseSqA  = val;
    //    NSLog(@"timeBaseSqA: %d", self.timeBaseSqA);
    if ([self loadIntParam:@"repeatsSqA"   into:&val notFound: 8]) doSave=true; self.repeatsSqA   = val;
    
    if ([self loadIntParam:@"timeBaseSqB"  into:&val notFound: 4]) doSave=true; self.timeBaseSqB  = val;
    //    NSLog(@"timeBaseSqB: %d", self.timeBaseSqB);
    if ([self loadIntParam:@"repeatsSqB"   into:&val notFound: 6]) doSave=true; self.repeatsSqB   = val;
    
    if ([self loadIntParam:@"timeBaseSqC"  into:&val notFound: 5]) doSave=true; self.timeBaseSqC  = val;
    //    NSLog(@"timeBaseSqC: %d", self.timeBaseSqC);
    if ([self loadIntParam:@"repeatsSqC"   into:&val notFound: 6]) doSave=true; self.repeatsSqC   = val;
    
    if (doSave)
        [self setNeedsSave:YES];
}
- (BOOL)loadIntParam:(NSString*)name_ into:(int*)val_ notFound:(int)default_ {
    if (![[NSUserDefaults standardUserDefaults] objectForKey:name_]) {
        *val_ = default_;
        return true;
    } else {
        *val_ = (int)[[NSUserDefaults standardUserDefaults] integerForKey:name_];
        return false;
    }
}

@end
