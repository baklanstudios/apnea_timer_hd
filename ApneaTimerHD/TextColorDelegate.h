//
//  TextColorDelegate.h
//  ApneaTimerHD
//
//  Created by Mikhail on 15.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#ifndef ApneaTimerHD_TextColorDelegate_h
#define ApneaTimerHD_TextColorDelegate_h

@protocol TextColorDelegate /*<NSObject>*/

-(void) setDurationColor:(UIColor*) color1 andDescriptionColor:(UIColor*) color2;

@end

#endif
