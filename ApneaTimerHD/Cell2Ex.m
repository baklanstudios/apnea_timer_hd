//
//  Cell2Ex.m
//  ApneaTimerHD
//
//  Created by Mikhail on 12.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "Cell2Ex.h"

@implementation Cell2Ex

- (void)initSelf {
    [super initSelf];
    self.lblCycles = [self createCyclesLabel];
}

- (void) layout {
    NSDictionary *views = @{@"tit1" : self.lblDescription1,
                            @"tit2" : self.lblDescription2,
                            @"dur1" : self.lblDuration1,
                            @"dur2" : self.lblDuration2,
                            @"cyc"  : self.lblCycles};
    
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dur1][tit1(==dur1)]|"
                                                                   options: 0
                                                                   metrics:nil
                                                                     views:views];
    [self.contentView addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dur2][tit2(==dur2)]|"
                                                          options: 0
                                                          metrics:nil
                                                            views:views];
    [self.contentView addConstraints:constraints];

    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cyc]|"
                                                          options: 0
                                                          metrics:nil
                                                            views:views];
    [self.contentView addConstraints:constraints];

    int w = self.contentView.superview.frame.size.width / 5;
    NSString *str = [NSString stringWithFormat:@"H:|[dur1][dur2(==dur1)][cyc(==%d)]|", w];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:str
                                                          options: 0
                                                          metrics:nil
                                                            views:views];
    [self.contentView addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tit1][tit2(==tit1)][cyc]|"
                                                          options: 0
                                                          metrics:nil
                                                            views:views];
    [self.contentView addConstraints:constraints];
}

-(void) setDurationColor:(UIColor*) color1 andDescriptionColor:(UIColor*) color2 {
    self.lblDuration1.textColor    = color1;
    self.lblDescription1.textColor = color2;
    self.lblDuration2.textColor    = color1;
    self.lblDescription2.textColor = color2;
    self.lblCycles.textColor       = color1;
}

@end
