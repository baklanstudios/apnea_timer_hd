//
//  Cell3.m
//  ApneaTimerHD
//
//  Created by Mikhail on 12.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "Cell3.h"

@implementation Cell3

- (void)initSelf {
    self.lblDescription1 = [self createDescriptionLabel];
    self.lblDescription2 = [self createDescriptionLabel];
    self.lblDescription3 = [self createDescriptionLabel];
    self.lblDuration1    = [self createDurationLabel];
    self.lblDuration2    = [self createDurationLabel];
    self.lblDuration3    = [self createDurationLabel];
}

-(void) layout {
    NSDictionary *views = @{@"tit1" : self.lblDescription1,
                            @"tit2" : self.lblDescription2,
                            @"tit3" : self.lblDescription3,
                            @"dur1" : self.lblDuration1,
                            @"dur2" : self.lblDuration2,
                            @"dur3" : self.lblDuration3};
    
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dur1][tit1(==dur1)]|"
                                                                   options: 0
                                                                   metrics:nil
                                                                     views:views];
    [self.contentView addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dur2][tit2(==dur2)]|"
                                                          options: 0
                                                          metrics:nil
                                                            views:views];
    [self.contentView addConstraints:constraints];

    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dur3][tit3(==dur3)]|"
                                                          options: 0
                                                          metrics:nil
                                                            views:views];
    [self.contentView addConstraints:constraints];

    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dur1][dur2(==dur1)][dur3(==dur2)]|"
                                                          options: 0
                                                          metrics:nil
                                                            views:views];
    [self.contentView addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tit1][tit2(==tit1)][tit3(==tit2)]|"
                                                          options: 0
                                                          metrics:nil
                                                            views:views];
    [self.contentView addConstraints:constraints];
}

-(void) setDurationColor:(UIColor*) color1 andDescriptionColor:(UIColor*) color2 {
    self.lblDuration1.textColor    = color1;
    self.lblDescription1.textColor = color2;
    self.lblDuration2.textColor    = color1;
    self.lblDescription2.textColor = color2;
    self.lblDuration3.textColor    = color1;
    self.lblDescription3.textColor = color2;
}

@end
