//
//  MenuViewController.h
//  ApneaTimerHD
//
//  Created by Mikhail on 08.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "TableSelectionDelegate.h"

@interface MenuViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) id<TableSelectionDelegate> delegate;
@property (weak, nonatomic) AppDelegate *appDelegate;

-(void)setDataTable:(NSArray*)tableNames;

@end
