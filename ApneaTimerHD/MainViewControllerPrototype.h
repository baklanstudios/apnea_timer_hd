//
//  MainViewControllerPrototype.h
//  ApneaTimerHD
//
//  Created by Mikhail on 08.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

@class AdBannerDelegate;
@class AppDelegate;

@protocol MainViewControllerDelegate

-(void) hideBanner;
-(void) showBanner;

@end


@interface MainViewControllerPrototype : UIViewController <ADBannerViewDelegate, UIActionSheetDelegate, MainViewControllerDelegate>

@property (weak, nonatomic)   AppDelegate      *appDelegate;
@property (weak, nonatomic)   AdBannerDelegate *bannerDelegate;
@property (strong, nonatomic) UIView           *adContainer;

-(void) addSubViewConstraint:(NSString*)str
                      toView:target
                    withDict:dict;

-(void) setButtonsEnabled:(BOOL)enabled;

@end
