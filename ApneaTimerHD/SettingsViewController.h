//
//  SettingsViewController.h
//  ApneaTimerHD
//
//  Created by Mikhail on 17.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

#import "MainViewControllerPrototype.h"

@interface SettingsViewController : UIViewController <SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property (weak, nonatomic) id<MainViewControllerDelegate> delegate;

@end
