//
//  CellCommon.h
//  ApneaTimerHD
//
//  Created by Mikhail on 12.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextColorDelegate.h"

@interface CellCommon : UITableViewCell <TextColorDelegate>

-(void) initSelf;
-(void) layout;

-(UILabel*) createDescriptionLabel;
-(UILabel*) createDurationLabel;
-(UILabel*) createCyclesLabel;

@end
