//
//  AppDelegate.h
//  ApneaTimerHD
//
//  Created by Mikhail on 08.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableSelectionDelegate.h"
#import "CSPausibleTimer.h"

@class TrainingTable;
@class AdBannerDelegate;
@class MainViewControllerTimer;
@class MainViewControllerSplash;
@class MFSideMenuContainerViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, TableSelectionDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (weak, nonatomic)   MFSideMenuContainerViewController *container;
@property (strong, nonatomic) MainViewControllerTimer           *timerView;
@property (strong, nonatomic) MainViewControllerSplash          *splashView;

@property (weak, nonatomic)   TrainingTable    *trainingTable;

#pragma mark - ...
@property (strong, nonatomic) CSPausibleTimer    *timer;

#pragma mark - Application Params

@property BOOL purchased;
@property BOOL useSound;

@property int  timeBaseO2;
@property int  timeBaseCO2;

@property int  timeBaseTriA;
@property int  repeatsTriA;

@property int  timeBaseTriB;
@property int  repeatsTriB;

@property int  timeBaseSqA;
@property int  repeatsSqA;

@property int  timeBaseSqB;
@property int  repeatsSqB;

@property int  timeBaseSqC;
@property int  repeatsSqC ;

@property int  tableIndex;

@property BOOL needsSave;
-(void) setTimeBase:(int)val;
-(void) setRepeats:(int)val;


#pragma mark - Banner
@property (strong, nonatomic) AdBannerDelegate *bannerDelegate;

#pragma mark - Utility Functions
- (void) saveSettings;
//- (void) hideAdBanner;

@end

