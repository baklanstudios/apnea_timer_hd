//
//  Cell2Ex.h
//  ApneaTimerHD
//
//  Created by Mikhail on 12.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "Cell2.h"

static NSString * const idCell2Ex = @"Cell2Ex";

@interface Cell2Ex : Cell2

@property (nonatomic, strong) UILabel *lblCycles;

@end
