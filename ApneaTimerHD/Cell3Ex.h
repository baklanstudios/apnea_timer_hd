//
//  Cell3Ex.h
//  ApneaTimerHD
//
//  Created by Mikhail on 12.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "Cell3.h"

static NSString * const idCell3Ex = @"Cell3Ex";

@interface Cell3Ex : Cell3

@property (nonatomic, strong) UILabel *lblCycles;

@end
