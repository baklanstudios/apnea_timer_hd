//
//  CellManager.h
//  ApneaTimerHD
//
//  Created by Mikhail on 12.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class TrainingTable;

@interface CellManager : NSObject

+ (void) registerCellTypes:(UITableView*)tv;

+ (UITableViewCell*) getTrainingTableCell:(TrainingTable*)tt
                                tableView:(UITableView *)tv
                                      idx:(NSIndexPath *)indexPath;

@end
