//
//  ViewController.m
//  ApneaTimerHD
//
//  Created by Mikhail on 08.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "MainViewControllerSplash.h"
#import "MFSideMenu.h"


@interface MainViewControllerSplash ()

@property (weak, nonatomic) IBOutlet UIImageView *splashImage;

@end

@implementation MainViewControllerSplash

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) updateViewConstraints {
    [super updateViewConstraints];
    
    NSArray *constraints;
    
    constraints = [NSLayoutConstraint
                            constraintsWithVisualFormat:@"H:[view]-(<=1)-[image]"
                            options:NSLayoutFormatAlignAllCenterY
                            metrics:nil
                            views: @{@"image":self.splashImage, @"view":self.view}];
    
    [self.view addConstraints:constraints];
    
    constraints = [NSLayoutConstraint
                            constraintsWithVisualFormat:@"V:[view]-(<=1)-[image]"
                            options:NSLayoutFormatAlignAllCenterX
                            metrics:nil
                            views: @{@"image":self.splashImage, @"view":self.view}];
    
    [self.view addConstraints:constraints];
}

@end
