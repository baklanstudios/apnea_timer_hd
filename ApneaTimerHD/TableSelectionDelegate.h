//
//  TableSelectionDelegate.h
//  ApneaTimerHD
//
//  Created by Mikhail on 08.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#ifndef ApneaTimerHD_TableSelectionDelegate_h
#define ApneaTimerHD_TableSelectionDelegate_h

@protocol TableSelectionDelegate /*<NSObject>*/

-(void)selectedTableName:(NSString*)tableName;
-(void)selectedTableIndex:(int)index;

@end

#endif
