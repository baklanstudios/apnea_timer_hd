//
//  CellSimple.m
//  ApneaTimerHD
//
//  Created by Mikhail on 12.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "CellSimple.h"

@implementation CellSimple

- (void)initSelf {
    self.lblDescription = [self createDescriptionLabel];
    self.lblDuration    = [self createDurationLabel];
}

-(void) layout {
    NSDictionary *views = @{@"title" : self.lblDescription, @"duration" : self.lblDuration};
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[duration][title(==duration)]|"
                                                                   options: 0
                                                                   metrics:nil
                                                                     views:views];
    [self.contentView addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[duration]|"
                                                          options: 0
                                                          metrics:nil
                                                            views:views];
    [self.contentView addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[title]|"
                                                          options: 0
                                                          metrics:nil
                                                            views:views];
    [self.contentView addConstraints:constraints];
}

-(void) setDurationColor:(UIColor*) color1 andDescriptionColor:(UIColor*) color2 {
    self.lblDuration.textColor    = color1;
    self.lblDescription.textColor = color2;
}

@end
