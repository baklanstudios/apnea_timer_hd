//
//  CellManager.m
//  ApneaTimerHD
//
//  Created by Mikhail on 12.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "AppDelegate.h"
#import "CellManager.h"
#import "Tools.h"

#import "TrainingTable.h"
#import "TrainingTableEx.h"

#import "CellSimple.h"
#import "Cell2.h"
#import "Cell2Ex.h"
#import "Cell3.h"
#import "Cell3Ex.h"
#import "Cell4.h"
#import "Cell4Ex.h"

@implementation CellManager

+(void) registerCellTypes:(UITableView*)tv {
    [tv registerClass:[CellSimple class] forCellReuseIdentifier:idCellSimple];
    [tv registerClass:[Cell2      class] forCellReuseIdentifier:idCell2     ];
    [tv registerClass:[Cell2Ex    class] forCellReuseIdentifier:idCell2Ex   ];
    [tv registerClass:[Cell3      class] forCellReuseIdentifier:idCell3     ];
    [tv registerClass:[Cell3Ex    class] forCellReuseIdentifier:idCell3Ex   ];
    [tv registerClass:[Cell4      class] forCellReuseIdentifier:idCell4     ];
    [tv registerClass:[Cell4Ex    class] forCellReuseIdentifier:idCell4Ex   ];
}

+(UITableViewCell*) getTrainingTableCell:(TrainingTable*)tt
                               tableView:(UITableView *)tv
                                     idx:(NSIndexPath *)indexPath {

    if (tt.phases.count == 2) {
        if ([tt isKindOfClass:[TrainingTableEx class]]) {
            return [self getCell2Ex:tv idx:indexPath table:tt];
        } else {
            return [self getCell2:tv idx:indexPath table:tt];
        }
    } else if (tt.phases.count == 3) {
        if ([tt isKindOfClass:[TrainingTableEx class]]) {
            return [self getCell3Ex:tv idx:indexPath table:tt];
        } else {
            return [self getCell3:tv idx:indexPath table:tt];
        }
    } else if (tt.phases.count == 4) {
        if ([tt isKindOfClass:[TrainingTableEx class]]) {
            return [self getCell4Ex:tv idx:indexPath table:tt];
        } else {
            return [self getCell4:tv idx:indexPath table:tt];
        }
    } else {
//        if ([tt isKindOfClass:[TrainingTableEx class]]) {
//            return [self getCellSimple:tv idx:indexPath];
//        } else {
            return [self getCellSimple:tv idx:indexPath table:tt];
//        }
    }
}

+(UITableViewCell*) getCellSimple:(UITableView *)tv idx:(NSIndexPath *)idx table:(TrainingTable*)table {

     CellSimple *cell = [tv dequeueReusableCellWithIdentifier:idCellSimple forIndexPath:idx];
     cell.lblDescription.text = [table getPhaseAbbreviation:0];
     cell.lblDuration.text    = [Tools secondsToTimeStr:[table getPhaseDuration:0 forStep:(int)idx.row]];
    return cell;
}

+(UITableViewCell*) getCell2:(UITableView *)tv idx:(NSIndexPath *)idx table:(TrainingTable*)table {
    Cell2 *cell = [tv dequeueReusableCellWithIdentifier:idCell2 forIndexPath:idx];
    cell.lblDescription1.text = [table getPhaseAbbreviation:0];
    cell.lblDuration1.text    = [Tools secondsToTimeStr:[table getPhaseDuration:0 forStep:(int)idx.row]];
    cell.lblDescription2.text = [table getPhaseAbbreviation:1];
    cell.lblDuration2.text    = [Tools secondsToTimeStr:[table getPhaseDuration:1 forStep:(int)idx.row]];
    return cell;
}

+(UITableViewCell*) getCell2Ex:(UITableView *)tv idx:(NSIndexPath *)idx table:(TrainingTable*)table {
    Cell2Ex *cell = [tv dequeueReusableCellWithIdentifier:idCell2Ex forIndexPath:idx];
    cell.lblDescription1.text = [table getPhaseAbbreviation:0];
    cell.lblDuration1.text    = [Tools secondsToTimeStr:[table getPhaseDuration:0 forStep:(int)idx.row]];
    cell.lblDescription2.text = [table getPhaseAbbreviation:1];
    cell.lblDuration2.text    = [Tools secondsToTimeStr:[table getPhaseDuration:1 forStep:(int)idx.row]];

    TrainingTableEx *tex      = (TrainingTableEx *)table;
    cell.lblCycles.text       = [NSString stringWithFormat:@"x%d", [tex.cyclesInitial[idx.row] intValue]];
    
    return cell;
}

+(UITableViewCell*) getCell3:(UITableView *)tv idx:(NSIndexPath *)idx table:(TrainingTable*)table {
    Cell3 *cell = [tv dequeueReusableCellWithIdentifier:idCell3 forIndexPath:idx];
    cell.lblDescription1.text = [table getPhaseAbbreviation:0];
    cell.lblDuration1.text    = [Tools secondsToTimeStr:[table getPhaseDuration:0 forStep:(int)idx.row]];
    cell.lblDescription2.text = [table getPhaseAbbreviation:1];
    cell.lblDuration2.text    = [Tools secondsToTimeStr:[table getPhaseDuration:1 forStep:(int)idx.row]];
    cell.lblDescription3.text = [table getPhaseAbbreviation:2];
    cell.lblDuration3.text    = [Tools secondsToTimeStr:[table getPhaseDuration:2 forStep:(int)idx.row]];
    return cell;
}

+(UITableViewCell*) getCell3Ex:(UITableView *)tv idx:(NSIndexPath *)idx table:(TrainingTable*)table {
    Cell3Ex *cell = [tv dequeueReusableCellWithIdentifier:idCell3Ex forIndexPath:idx];
    cell.lblDescription1.text = [table getPhaseAbbreviation:0];
    cell.lblDuration1.text    = [Tools secondsToTimeStr:[table getPhaseDuration:0 forStep:(int)idx.row]];
    cell.lblDescription2.text = [table getPhaseAbbreviation:1];
    cell.lblDuration2.text    = [Tools secondsToTimeStr:[table getPhaseDuration:1 forStep:(int)idx.row]];
    cell.lblDescription3.text = [table getPhaseAbbreviation:2];
    cell.lblDuration3.text    = [Tools secondsToTimeStr:[table getPhaseDuration:2 forStep:(int)idx.row]];
    
    TrainingTableEx *tex      = (TrainingTableEx *)table;
    cell.lblCycles.text       = [NSString stringWithFormat:@"x%d", [tex.cyclesInitial[idx.row] intValue]];
    
    return cell;
}

+(UITableViewCell*) getCell4:(UITableView *)tv idx:(NSIndexPath *)idx table:(TrainingTable*)table {
    Cell4 *cell = [tv dequeueReusableCellWithIdentifier:idCell4 forIndexPath:idx];
    cell.lblDescription1.text = [table getPhaseAbbreviation:0];
    cell.lblDuration1.text    = [Tools secondsToTimeStr:[table getPhaseDuration:0 forStep:(int)idx.row]];
    cell.lblDescription2.text = [table getPhaseAbbreviation:1];
    cell.lblDuration2.text    = [Tools secondsToTimeStr:[table getPhaseDuration:1 forStep:(int)idx.row]];
    cell.lblDescription3.text = [table getPhaseAbbreviation:2];
    cell.lblDuration3.text    = [Tools secondsToTimeStr:[table getPhaseDuration:2 forStep:(int)idx.row]];
    cell.lblDescription4.text = [table getPhaseAbbreviation:3];
    cell.lblDuration4.text    = [Tools secondsToTimeStr:[table getPhaseDuration:3 forStep:(int)idx.row]];
    return cell;
}

+(UITableViewCell*) getCell4Ex:(UITableView *)tv idx:(NSIndexPath *)idx table:(TrainingTable*)table {
    Cell4Ex *cell = [tv dequeueReusableCellWithIdentifier:idCell4Ex forIndexPath:idx];
    cell.lblDescription1.text = [table getPhaseAbbreviation:0];
    cell.lblDuration1.text    = [Tools secondsToTimeStr:[table getPhaseDuration:0 forStep:(int)idx.row]];
    cell.lblDescription2.text = [table getPhaseAbbreviation:1];
    cell.lblDuration2.text    = [Tools secondsToTimeStr:[table getPhaseDuration:1 forStep:(int)idx.row]];
    cell.lblDescription3.text = [table getPhaseAbbreviation:2];
    cell.lblDuration3.text    = [Tools secondsToTimeStr:[table getPhaseDuration:2 forStep:(int)idx.row]];
    cell.lblDescription4.text = [table getPhaseAbbreviation:3];
    cell.lblDuration4.text    = [Tools secondsToTimeStr:[table getPhaseDuration:3 forStep:(int)idx.row]];
    TrainingTableEx *tex      = (TrainingTableEx *)table;
    cell.lblCycles.text       = [NSString stringWithFormat:@"x%d", [tex.cyclesInitial[idx.row] intValue]];
    
    return cell;
}

@end
