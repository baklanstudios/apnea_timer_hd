//
//  CellCommon.m
//  ApneaTimerHD
//
//  Created by Mikhail on 12.01.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "Tools.h"

#import "CellCommon.h"

@implementation CellCommon

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSelf];
        [self layout];
    }
    return self;
}

-(void) initSelf {
    mustOverride(); // or methodNotImplemented(), same thing
}

-(void) layout {
    mustOverride(); // or methodNotImplemented(), same thing
}

-(UILabel*) createDescriptionLabel {
    UILabel *label = [[UILabel alloc] init];
    [label setTextColor:[UIColor grayColor]];
    [label setFont:[UIFont systemFontOfSize:18]];
    [label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [label setTextAlignment:NSTextAlignmentCenter];
//    [self.lblDescription setBackgroundColor:[UIColor yellowColor]];
    [self.contentView addSubview:label];
    return label;
}

-(UILabel*) createDurationLabel {
    UILabel *label = [[UILabel alloc] init];
    [label setTextColor:[UIColor blackColor]];
    [label setFont:[UIFont systemFontOfSize:22]];
    [label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [label setTextAlignment:NSTextAlignmentCenter];
//    [self.lblDescription setBackgroundColor:[UIColor yellowColor]];
    [self.contentView addSubview:label];
    return label;
}

-(UILabel*) createCyclesLabel {
    UILabel *label = [[UILabel alloc] init];
    [label setTextColor:[UIColor blackColor]];
    [label setFont:[UIFont systemFontOfSize:22]];
    [label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [label setTextAlignment:NSTextAlignmentCenter];
//    [label setBackgroundColor:[UIColor yellowColor]];
    [self.contentView addSubview:label];
    return label;
}

@end
